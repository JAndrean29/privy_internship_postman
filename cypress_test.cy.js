/// <reference types="cypress" />

describe('Codedamn website test', () => {

  //Fungsi yang akan berjalan tiap iterasi pengetesan (it)
  //jadi browser akan selalu melakukan visit sebelum menjalankan it test
  //tanpa harus memasukan cy.visit pada tiap it test.
  beforeEach(() => {
    cy.visit('https://codedamn.com/')
  })


  //test untuk memeriksa apakah website sudah dimuat dengan baik hingga akhir
  //cy contains mengecek beberapa kata/kalimat pada beberapa lokasi
  //dengan contains, ke-4 akan memeriksa kalimat yang ada pada footer website
  it('Website loads fine', () => {
    cy.contains('Learn Programming').should('exist')
    cy.contains('Build projects, practice and learn to code from scratch - without leaving your browser.').should('exist')
    cy.contains('Become the best programmer you can be').should('exist')
    cy.contains('Learn to code interactively - without ever leaving your browser.').should('exist')
    cy.log('expected contents found, website loaded')
    cy.pause()
  })


  //test untuk melakukan pengecekkan elemen
  //dan melakukan navigasi ke halaman sign in
  //serta mencoba melakukan test untuk mengisi sebuah form menggunakan cy.type()
  //lalu mencoba melakukan log in dengan mencari elemen button Sign In
  it('Access login page and try typing and login', () => {
    cy.log('Navigating to Login Page')
    cy.contains('Sign In').click()
    cy.url().should('include', '/login')
    cy.get('div > [data-testid=username]').should('exist')
    cy.get('[data-testid=username]').click().type('JAndrean29')
    cy.get('div > [data-testid=password]').should('exist')
    cy.get('[data-testid=password]').click().type('loremipsum')
    cy.get('[data-testid=login]').should('exist').click()
    cy.url().should('include', '/dashboard') //dashboard adalah expected page apabila log in berhasil
    cy.pause()
    //both data are dummy and is not submitted
  })

  
  //test untuk membuka halaman sign in
  //lalu mengakses password-reset dengan mencari kata Forgot your password
  //dan memastikan kalimat ada pada halaman menggunakan should function
  //lalu melakukan click pada forgot your password untuk mengakses password-reset
  it('Access login page then forgot password', () => {
    cy.log('Navigating to Login Page')
    cy.contains('Sign In').click()
    cy.url().should('include', '/login')
    cy.contains('Forgot your password?').should('exist')
    cy.contains('Forgot your password?').click()
    cy.url().should('include', '/password-reset')
    cy.pause()
  })


  //mengakses halaman register
  //dan kembali ke landing page menggunakan cy.go(back)
  it('Access Register Page', () => {
    cy.log('Navigating to Register Page')
    cy.contains('Sign Up').click()
    cy.url().should('include', '/register')
    cy.log('returning to landing page')
    cy.go('back')
    cy.url().should('eq', 'https://codedamn.com/')
    cy.pause()
  })

  //mengubah tampilan viewport menjadi model gadget tertentu
  //atau ukuran x, y yang diinginkan
  it('Viewport tests mobile view', () => {
    cy.viewport('iphone-6+')
  })
})