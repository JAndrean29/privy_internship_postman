Repository untuk Assignment A1 program internship PrivyPass, berisi:

File Json untuk Postman, dan cypress_test.cy.js

Link untuk postman akses workspace Postman tanpa harus import json:
https://www.postman.com/aviation-geoscientist-29118282/workspace/privy-internship-assignment-postman/collection/24877368-a97f2329-d81e-4081-8931-46f22ecafed8?action=share&creator=24877368

atau (memerlukan fork jadi kurang disarankan atau opsional jika ada kendala pada file json atau link)
[![Run in Postman](https://run.pstmn.io/button.svg)](https://god.gw.postman.com/run-collection/24877368-a97f2329-d81e-4081-8931-46f22ecafed8?action=collection%2Ffork&collection-url=entityId%3D24877368-a97f2329-d81e-4081-8931-46f22ecafed8%26entityType%3Dcollection%26workspaceId%3D04046811-a619-4c29-8088-83d3175678e7)
